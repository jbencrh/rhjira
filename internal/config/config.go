package config

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"strings"
	"syscall"

	"github.com/spf13/viper"
	"golang.org/x/crypto/ssh/terminal"
	"github.com/zalando/go-keyring"
)

// Get internal lab logger instance

// MainConfig represents the loaded config
var (
	// MainConfig represents the loaded config
	MainConfig *viper.Viper
	keyring_user string = "rhjira-user"
	keyring_service string = "rhjira"
	jiraTokenKeyring string = "rhjira-keyring"
)


// New prompts the user for the default config values to use with rhjira, and save
// them to the provided confpath (default: ~/.config/lab.hcl)
func New(confpath string, r io.Reader) error {
	var (
		reader                                   = bufio.NewReader(r)
		jiraToken, jiraURL, loadToken string
		err                                      error
	)

	confpath = path.Join(confpath, "rhjira.toml")


	// Set the Jira URL
	fmt.Printf("Enter Jira URL: ")
	jiraURL, err = reader.ReadString('\n')
	jiraURL = strings.TrimSpace(jiraURL)
	if err != nil {
		return err
	}

	MainConfig.Set("core.jiraurl", jiraURL)

	// Set the Jira Token
	fmt.Printf("Create a Jira token by following the instructions at https://id.atlassian.com/manage-profile/security/api-tokens.")
	fmt.Printf("Enter Jira Token:")

	byteToken, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return err
	}

	if strings.TrimSpace(string(byteToken)) == "" &&
	   strings.TrimSpace(loadToken) == "" {
		log.Fatal("Error: No bugzilla token provided")
	}

	jiraToken = strings.TrimSpace(string(byteToken))

	// set password
	err = keyring.Set(keyring_service, keyring_user, jiraToken)
	if err != nil {
		log.Printf("Warning: Unable to save jira token in keyring.  Token is saved as clear text in %s.\n", confpath)
		MainConfig.Set("core.jiratoken", jiraToken)
	} else {
		MainConfig.Set("core.jiratoken", jiraTokenKeyring)
	}

	if err := MainConfig.WriteConfigAs(confpath); err != nil {
		return err
	}
	fmt.Printf("\nrhjira config saved to %s\n", confpath)

	err = MainConfig.ReadInConfig()
	if err != nil {
		log.Fatal(err)
		UserConfigError()
	}
	return nil
}

func LoadMainConfig() (string, string) {

	var jiraRawConfigPath string

	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}

	// Try to find XDG_CONFIG_HOME which is declared in XDG base directory
	// specification and use it's location as the config directory
	confpath := os.Getenv("XDG_CONFIG_HOME")
	if confpath == "" {
		confpath = path.Join(home, ".config")
	}
	jiraRawConfigPath = confpath + "/rhjira"
	if _, err := os.Stat(jiraRawConfigPath); os.IsNotExist(err) {
		os.MkdirAll(jiraRawConfigPath, 0700)
	}

	MainConfig = viper.New()
	MainConfig.SetConfigName("rhjira.toml")
	MainConfig.SetConfigType("toml")

	MainConfig.AddConfigPath(jiraRawConfigPath)

	if _, ok := MainConfig.ReadInConfig().(viper.ConfigFileNotFoundError); ok {
		// Create a new config
		err := New(jiraRawConfigPath, os.Stdin)
		if err != nil {
			log.Fatal(err)
		}
	}

	if !MainConfig.IsSet("core.jiratoken") || !MainConfig.IsSet("core.jiraURL") {
		UserConfigError()
		return "", ""
	}

	// is the token saved in the keyring (hint: run seahorse to view keyring)
	jiraToken := MainConfig.GetString("core.jiratoken")
	if MainConfig.GetString("core.jiratoken") == jiraTokenKeyring {
		jiraToken, err = keyring.Get(keyring_service, keyring_user)
		if err != nil {
			log.Fatal("Unable to download rhjira from keyring. Hint: Use seahorse to see if the Jira Token was saved to your keyring.")
		}
	}
	return jiraToken, MainConfig.GetString("core.jiraurl")
}

// UserConfigError returns a default error message about authentication
func UserConfigError() {
	fmt.Println("Error: User authentication failed.  This is likely due to a misconfigured Jira Token.  Verify the jiratoken and jiraurl config settings before attempting to authenticate.  See https://id.atlassian.com/manage-profile/security/api-tokens")
	os.Exit(1)
}
