package main

import (
	"log"

	jira "github.com/andygrunwald/go-jira"
	"gitlab.com/prarit/rhjira/cmd"
	"gitlab.com/prarit/rhjira/internal/config"
)

func main() {

	var err error

	// Load the .env file
	jira_token, jira_url := config.LoadMainConfig()

	// Create a BasicAuth Transport object
	tp := jira.BearerAuthTransport{
		Token: jira_token,
	}
	// Create a new Jira Client
	client, err := jira.NewClient(tp.Client(), jira_url)
	if err != nil {
		log.Fatal(err)
	}

	me, _, err := client.User.GetSelf()
	if err != nil || me.DisplayName == "" {
		log.Fatal(err)
	}

	cmd.Execute(client)
}
