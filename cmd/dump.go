package cmd

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"time"

	jira "github.com/andygrunwald/go-jira"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/cobra"
	// This is useful with spew.Dump(struct), ie) spew.Dump(issue)
	"github.com/davecgh/go-spew/spew"
)

var (
	showCustomFieldIDs bool
	userSelectedFields []string
	noEscapedText      bool
	showEmptyFields    bool
	debug              bool
)

func outputField(name string, value interface{}) {
	var valueStr interface{}

	if len(userSelectedFields) != 0 && !contains(userSelectedFields, name) {
		return
	}

	switch valueType := value.(type) {
	case string:
		valueStr = valueType
	case int:
		valueStr = strconv.Itoa(valueType)
	case jira.Time:
		valueStr = time.Time(valueType).Format(timeFormat)
	case *jira.User:
		if valueType != nil {
			valueStr = valueType.DisplayName + " <" + valueType.Name + ">"
		} else {
			valueStr = ""
		}
	}

	fmt.Printf("FIELD[%s]: %s\n", name, valueStr)
}

func unknownFieldWarning(issue *jira.Issue, field jira.Field, err error) {
	fmt.Printf("\n-------- 8< cut here 8< --------\n")
	fmt.Println("Warning:", err)
	fmt.Println("Warning: UNKNOWN FIELD TYPE.  Please cut-and-paste this output and report this to https://gitlab.com/prarit/rhjira/-/issues")
	fmt.Printf("Issue ID: %s [%s]\n", issue.ID, time.Now().String())
	fmt.Printf(" The data is field.ID[%s] and schemaType[%s]\n",
		field.ID, field.Schema.Type)
	fmt.Printf(" Data dump for field.Name[%s]:\n", field.Name)
	spew.Dump(issue.Fields.Unknowns[field.ID])
	fmt.Println("-------- 8< end here 8< --------")
}

func dumpOption(entry interface{}) string {
	type option struct {
		Value    string
		Id       string
		Disabled bool // this means disabled in search
		Self     string
	}

	var u option
	err := mapstructure.Decode(entry, &u)
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%s", u.Value)
}

func dumpUser(entry interface{}) string {
	var u jira.User
	err := mapstructure.Decode(entry, &u)
	if err != nil {
		log.Fatal(err)
	}
	if u.EmailAddress != "" {
		return fmt.Sprintf("%s <%s>", u.Name, u.EmailAddress)
	}
	return fmt.Sprintf("%s", u.Name)
}

func dumpVotes(entry interface{}) string {
	type votes struct {
		HasVoted bool
		Self     string
		Votes    float64
	}

	var u votes
	err := mapstructure.Decode(entry, &u)
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%f", u.Votes)
}

func dumpSecurityLevel(entry interface{}) string {
	type securitylevel struct {
		Description string
		Id          string
		Name        string
		Self        string
	}

	var u securitylevel
	err := mapstructure.Decode(entry, &u)
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%s", u.Description)
}

func dumpVersion(entry interface{}) string {
	type version struct {
		Archived    bool
		Description string
		ID          string
		Name        string
		released    bool
		self        string
	}

	var u version
	err := mapstructure.Decode(entry, &u)
	if err != nil {
		log.Fatal(err)
	}
	return u.Name
}

func evaluateMSIfield(schemaType string, fieldID string, entry interface{}) (string, error) {
	switch schemaType {
	case "option":
		return dumpOption(entry), nil
	case "user":
		return dumpUser(entry), nil
	case "votes":
		return dumpVotes(entry), nil
	case "securitylevel":
		return dumpSecurityLevel(entry), nil
	case "any":
		// PRARIT: customfield_12316840/"Bugzilla Bug"
		if fieldID == "customfield_12316840" {
			type customfield_12316840 struct {
				SiteID float64
				BugID  float64
			}
			var u customfield_12316840
			err := mapstructure.Decode(entry, &u)
			if err != nil {
				log.Fatal(err)
			}
			return fmt.Sprintf("%d", int(u.BugID)), nil
		}
		return "", nil
	case "map[string]interface {}":
		// this is part of an array of values
		// PRARIT customfield_12311840/"Need Info From"
		if fieldID == "customfield_12311840" {
			return dumpUser(entry), nil
		}
	case "array":
		// this is part of an array of values
		// PRARIT customfield_12311840/"Need Info From"
		if fieldID == "customfield_12311840" {
			return dumpUser(entry), nil
		}
		// PRARIT customfield_12323140/"Target Version"
		// PRARIT FIXME: Why was this same code needed above in map[string]interface {}?
		if fieldID == "customfield_12323140" {
			return dumpVersion(entry), nil
		}
		return dumpOption(entry), nil
	case "issuelinks":
		if fieldID == "customfield_12318341" {
			type customfield_12318341 struct {
				Key    string
				fields interface{}
			}
			var u customfield_12318341
			err := mapstructure.Decode(entry, &u)
			if err != nil {
				log.Fatal(err)
			}
			return fmt.Sprintf("https://issues.redhat.com/browse/%s", u.Key), nil
		}
	} // end case statement

	return "", fmt.Errorf("schemaType=%s not handled in evaluateMSIfield()", schemaType)
}

func evaluateField(issue jira.Issue, fieldID string, entry interface{}, schemaType string) (string, error) {
	switch fieldType := entry.(type) {
	case string:
		// PRARIT customfield_12321740/"Testable Builds" uses returns instead of '\n'
		// which makes parsing very difficult.  Switch to using '\n' by default
		if fieldID == "customfield_12321740" {
			re := regexp.MustCompile(`\r?\n`)
			fieldType = re.ReplaceAllString(fieldType, "\\n")
		}
		return fmt.Sprintf("%v", fieldType), nil
	case float64:
		return fmt.Sprintf("%f", fieldType), nil
	case map[string]interface{}:
		return evaluateMSIfield(schemaType, fieldID, entry)
	case []interface{}:
		valueStr := ""
		for count, e := range fieldType {
			subStr, err := evaluateField(issue, fieldID, e, fmt.Sprintf("%T", e))
			if err != nil {
				return "", err
			}
			valueStr += fmt.Sprintf("%s", subStr)
			if count != (len(fieldType) - 1) {
				valueStr += ","
			}
		}
		return valueStr, nil
	}
	return "", fmt.Errorf("schemaType=%s not handled in evaluateField()\n", schemaType)
}

var dumpCmd = &cobra.Command{
	Use:     "dump",
	Short:   "Dump jira issue variables.",
	Aliases: []string{"raw"},
	Long:    ``,
	Args:    cobra.RangeArgs(1, 1),
	Run: func(cmd *cobra.Command, args []string) {

		if contains(args, "help") {
			cmd.Help()
			os.Exit(0)
		}

		issueID := args[0]

		issue, _, err := JiraClient.Sprint.GetIssue(issueID, nil)
		if err != nil {
			log.Fatal(err)
		}

		// this retrieves the list of fields for issues
		fieldList, _, err := JiraClient.Field.GetList()
		if err != nil {
			log.Fatal(err)
		}

		if debug {
			fmt.Println("issue +++++++++++++++++++++++++++++++++++++++++++++++++++")
			// the output of the issue.Fields.Unknowns contains the values
			// of all custom fields.  So there is no further lookup
			// necessary
			spew.Dump(issue)

			fmt.Println("fieldList ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
			spew.Dump(fieldList)

			fmt.Println("field.ID/Field.Name to schema type +++++++++++++++++++++++++++++++++++++++++++++++++++")
			fmt.Println("Search-Parameter Name Type")
			for _, field := range fieldList {
				fmt.Printf("%s \"%s\" %s\n", field.ID, field.Name, field.Schema.Type)
			}

			fmt.Println("Built-in field.ID/Field.Name to schema type +++++++++++++++++++++++++++++++++++++++++++")
			fmt.Println("Search-Parameter Name Type")
			for _, field := range fieldList {
				if field.Custom {
					continue
				}
				fmt.Printf("%s \"%s\" %s\n", field.ID, field.Name, field.Schema.Type)
			}

			fmt.Println("unknowns ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
		}

		outputField("Issue Key", issue.Key)
		for index, field := range fieldList {

			fieldName := field.Name
			if !field.Custom {
				fieldName = field.ID
			}
			if showCustomFieldIDs && field.Custom {
				fieldName = field.Name + " | " + field.ID
			}

			// if field is nil, output empty
			if issue.Fields.Unknowns[field.ID] == nil {
				if showEmptyFields {
					outputField(fieldName, "")
				}
				continue
			}

			var valueStr string
			switch issue.Fields.Unknowns[field.ID].(type) {
			case []interface{}:
				entries := issue.Fields.Unknowns[field.ID].([]interface{})
				for _, entry := range entries {
					valueStr, err = evaluateField(*issue, field.ID, entry, fieldList[index].Schema.Type)
					if err != nil {
						unknownFieldWarning(issue, field, err)
						outputField(fieldName, "")
						continue
					}
				}
			default:
				valueStr, err = evaluateField(*issue, field.ID, issue.Fields.Unknowns[field.ID], fieldList[index].Schema.Type)
				if err != nil {
					unknownFieldWarning(issue, field, err)
					outputField(fieldName, "")
					continue
				}
			}
			outputField(fieldName, valueStr)
		}

		//
		// Fields declared in Issue structure output
		//
		outputField("issuetype", GetIssueType(issue))
		outputField("project", GetProject(issue))
		outputField("resolutiondate", GetResolutionDate(issue))
		outputField("created", GetCreated(issue))
		outputField("watches", GetWatches(issue))
		outputField("assignee", GetAssignee(issue))
		outputField("updated", GetUpdated(issue))
		if noEscapedText {
			outputField("description", GetDescription(issue))
		} else {
			outputField("description", strconv.Quote(GetDescription(issue)))
		}
		outputField("summary", GetSummary(issue))
		outputField("creator", GetCreator(issue))
		outputField("reporter", GetReporter(issue))
		outputField("components", GetComponents(issue))
		outputField("status", GetStatus(issue))
		//outputField("statusCategory", issue.Fields.Status.StatusCategory.Name)
		// SKIP Progress
		// SKIP AggregateProgress
		// SKIP TimeTracking
		outputField("timespent", GetTimeSpent(issue))
		outputField("timeestimate", GetTimeEstimate(issue))
		// SKIP Worklog
		// SKIP IssueLink: TODO need an example of links
		if noEscapedText {
			outputField("comment", GetComments(issue))
		} else {
			outputField("comment", strconv.Quote(GetComments(issue)))
		}
		outputField("fixVersions", GetFixVersions(issue))
		outputField("affectsVersion", GetAffectsVersions(issue))
		outputField("labels", GetLabels(issue))
		// SKIP SubTasks
		// SKIP Attachments
		outputField("epic", GetEpic(issue))
		outputField("sprint", GetSprint(issue))
		outputField("parent", GetParent(issue))
		outputField("aggregatetimeoriginalestimate", GetAggregateTimeOriginalEstimate(issue))
		outputField("aggregatetimespent", GetAggregateTimeSpent(issue))
		outputField("aggregatetimeestimate", GetAggregateTimeEstimate(issue))
		outputField("remotelinks", GetRemoteLinks(issue))
	}, // end of func(cmd *cobra.Command, args []string)
}

func init() {
	dumpCmd.Flags().BoolVarP(&showCustomFieldIDs, "showcustomfields", "", false, "show the customfield IDs and the customfield names")
	dumpCmd.Flags().BoolVarP(&showEmptyFields, "showemptyfields", "", false, "show all fields including those that are not defined for this issue")
	dumpCmd.Flags().StringSliceVarP(&userSelectedFields, "fields", "", []string{}, "specify a comma-separated list of fields for output")
	dumpCmd.Flags().BoolVarP(&noEscapedText, "noescapedtext", "", false, "show dump text (ie, no escaped characters)")
	dumpCmd.Flags().BoolVarP(&debug, "debug", "d", false, "verbose debug output")
	dumpCmd.Flags().MarkHidden("debug")

	RootCmd.AddCommand(dumpCmd)
}
