package cmd

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
	"unicode"

	jira "github.com/andygrunwald/go-jira"
	"github.com/ryanuber/columnize"
	"github.com/spf13/cobra"

	"golang.org/x/crypto/ssh/terminal"
)

var (
	userIssueSelectedFields []string
	noHeader                bool
	useAcronyms             bool
	summaryLengthStr        string
	noLineNumber            bool
	rawOutput               bool
)

func addLink(line string, issueID string) string {
	return strings.Replace(line, issueID, link(issueID, fmt.Sprintf("https://issues.redhat.com/browse/%s", issueID)), -1)
}

func dumpIssueStructField(fieldID string, issue *jira.Issue) string {
	switch fieldID {
	case "issuetype":
		return GetIssueType(issue)
	case "project":
		return GetProject(issue)
	case "resolutiondate":
		return time.Time(GetResolutionDate(issue)).Format(timeFormat)
	case "created":
		return time.Time(GetCreated(issue)).Format(timeFormat)
	case "watches:":
		return GetWatches(issue)
	case "assignee":
		user := GetAssignee(issue)
		return user.DisplayName + " <" + user.Name + ">"
	case "updated":
		return time.Time(GetUpdated(issue)).Format(timeFormat)
	case "description":
		return strconv.Quote(GetDescription(issue))
	case "summary":
		return GetSummary(issue)
	case "creator":
		user := GetCreator(issue)
		return user.DisplayName + " <" + user.Name + ">"
	case "reporter":
		user := GetReporter(issue)
		return user.DisplayName + " <" + user.Name + ">"
	case "components":
		return GetComponents(issue)
	case "status":
		return GetStatus(issue)
	//case "statusCategory":
	//return issue.Fields.Status.StatusCategory.Name
	// SKIP Progress
	// SKIP AggregateProgress
	// SKIP TimeTracking
	case "timespent":
		return GetTimeSpent(issue)
	case "timeestimate":
		return GetTimeEstimate(issue)
	// SKIP Worklog
	// SKIP IssueLink: TODO need an example of links
	case "comment":
		return strconv.Quote(GetComments(issue))
	case "fixVersions":
		return GetFixVersions(issue)
	case "versions":
		return GetAffectsVersions(issue)
	case "labels":
		return GetLabels(issue)
	// SKIP SubTasks
	// SKIP Attachments
	case "epic":
		return GetEpic(issue)
	case "sprint":
		return GetSprint(issue)
	case "parent":
		return GetParent(issue)
	case "aggregatetimeoriginalestimate":
		return GetAggregateTimeOriginalEstimate(issue)
	case "aggregatetimespent":
		return GetAggregateTimeSpent(issue)
	case "aggregatetimeestimate":
		return GetAggregateTimeEstimate(issue)
	}
	return ""
}

func getOutputLine(issue jira.Issue, field jira.Field) string {
	if !field.Custom {
		return dumpIssueStructField(field.ID, &issue)
	}

	var (
		valueStr string
		err      error
	)

	switch issue.Fields.Unknowns[field.ID].(type) {
	case []interface{}:
		entries := issue.Fields.Unknowns[field.ID].([]interface{})
		for _, entry := range entries {
			valueStr, err = evaluateField(issue, field.ID, entry, field.Schema.Type)
			if err != nil {
				valueStr = "---"
				continue
			}
		}
	default:
		valueStr, err = evaluateField(issue, field.ID, issue.Fields.Unknowns[field.ID], field.Schema.Type)
		if err != nil {
			valueStr = "---"
		}
	}

	return valueStr
}

func jqlError(errStr string, jql string) {
	errStrSlice := strings.Split(errStr[strings.Index(errStr, "(")+1:strings.Index(errStr, ")")], " ")
	pointerLocation := -1
	for count, errSlice := range errStrSlice {
		if count < 3 {
			continue
		}
		pointerLocation, _ = strconv.Atoi(errSlice)
		break // shouldn't be necessary but you never know
	}

	fmt.Println(jql)
	for i := 1; i < pointerLocation; i++ {
		fmt.Printf(" ")
	}
	fmt.Printf("^\n")

	// "Error in the JQL Query: " is 24 characters
	log.Fatal(errStr[24:strings.Index(errStr, "(")])
}

var issueListCmd = &cobra.Command{
	Use:   "list",
	Short: "list issues",
	Long:  ``,
	Args:  cobra.RangeArgs(0, 1),
	Run: func(cmd *cobra.Command, args []string) {

		var (
			lastIssue int = 0
			issueList []jira.Issue
			jql       string = ""
			output    []string
			line      string
		)

		if contains(args, "help") {
			cmd.Help()
			os.Exit(0)
		}

		summaryLength, err := strconv.Atoi(summaryLengthStr)
		if err != nil {
			log.Fatal(err)
		}

		if len(args) != 0 {
			jql = strings.Join(args, " ")
		}

		for {
			opt := &jira.SearchOptions{
				MaxResults: 1000,
				StartAt:    lastIssue,
			}
			issues, resp, err := JiraClient.Issue.Search(jql, opt)
			if err != nil {
				if resp.StatusCode == 400 && strings.HasPrefix(err.Error(), "Error in the JQL Query:") {
					jqlError(err.Error(), jql)
				}
				log.Fatal(err)
			}

			total := resp.Total
			if issues == nil {
				issueList = make([]jira.Issue, 0, total)
			}

			issueList = append(issueList, issues...)
			lastIssue = resp.StartAt + len(issues)
			if lastIssue >= total {
				break
			}
		}

		if len(issueList) == 0 {
			log.Println("No issues found.")
			os.Exit(0)
		}

		lineDatas := make(map[int]string)

		// Default display is #, Issue, and Summary
		if len(userIssueSelectedFields) == 0 {
			userIssueSelectedFields = append(userIssueSelectedFields, "summary")
		}

		fieldList, _, err := JiraClient.Field.GetList()
		if err != nil {
			log.Fatal(err)
		}

		if !noHeader {
			// build header
			if noLineNumber {
				line = fmt.Sprintf("Issue")
			} else {
				line = fmt.Sprintf("#|Issue")
			}
			for _, userSelectedField := range userIssueSelectedFields {
				if !useAcronyms || !strings.Contains(userSelectedField, " ") {
					line += fmt.Sprintf("|%s", userSelectedField)
					continue
				}
				outputField := ""
				for _, char := range userSelectedField {
					if !unicode.IsLower(char) && char != ' ' {
						outputField += fmt.Sprintf("%c", char)
					}
				}
				line += fmt.Sprintf("|%s", outputField)
			}
			line += fmt.Sprintf("\n")
			// the header has no issue link, so set it to empty
			lineDatas[0] = ""
			output = append(output, line)
		}

		for count, issue := range issueList {
			line = ""
			if noLineNumber {
				line += fmt.Sprintf("%s", issue.Key)
			} else {
				line += fmt.Sprintf("%d|%s", count, issue.Key)
			}
			for _, userSelectedField := range userIssueSelectedFields {
				if userSelectedField == "remotelinks" {
					line += fmt.Sprintf("|%s", GetRemoteLinks(&issue))
					continue
				}
				for _, field := range fieldList {
					if field.ID == userSelectedField || field.Name == userSelectedField {
						lineDatas[count+1] = issue.Key
						if field.ID == "summary" && summaryLength != 0 {
							slen := getOutputLine(issue, field)
							if len(slen) > summaryLength {
								slen = slen[:summaryLength]
							}
							line += fmt.Sprintf("|%s", slen)
							continue
						}
						line += fmt.Sprintf("|%s", getOutputLine(issue, field))
					}
				}
			}
			line += fmt.Sprintf("\n")
			output = append(output, line)
		}

		//
		// Output
		//

		if rawOutput {
			for _, line := range output {
				fmt.Printf("%s", line)
			}
			os.Exit(0)
		}

		// columnize the output
		config := columnize.DefaultConfig()
		config.Delim = "|"
		result := columnize.Format(output, config)

		o, _ := os.Stdout.Stat()
		if (o.Mode() & os.ModeCharDevice) == os.ModeCharDevice { // terminal
			// width is for _this_ terminal
			width, _, err := terminal.GetSize(int(os.Stdin.Fd()))
			if err != nil {
				log.Fatal(err)
			}

			for count, line := range strings.Split(result, "\n") {
				if len(line) > width {
					line = line[:width]
				}
				fmt.Println(addLink(line, lineDatas[count]))
			}
		} else { // pipe
			fmt.Printf(result)
		}
	},
}

func init() {
	issueListCmd.Flags().StringSliceVarP(&userIssueSelectedFields, "fields", "", []string{}, "specify a comma-separated list of fields for output")
	issueListCmd.Flags().BoolVarP(&useAcronyms, "useacronyms", "", false, "use acronyms in the output header")
	issueListCmd.Flags().BoolVarP(&noHeader, "noheader", "", false, "do not display an output header")
	issueListCmd.Flags().StringVarP(&summaryLengthStr, "summarylength", "", "50", "Length of the summary string (default:50, do not truncate:0 )")
	issueListCmd.Flags().BoolVarP(&noLineNumber, "nolinenumber", "", false, "do not leading line number")
	issueListCmd.Flags().BoolVarP(&rawOutput, "rawoutput", "", false, "do not make output pretty (useful for scripts)")

	RootCmd.AddCommand(issueListCmd)
}
